-- ysrivastava
import Text.Printf
import System.IO

toFloat :: String -> Float
toFloat x = read x :: Float

toInt :: Float -> Int
toInt = round

-- Multiply given change by 100 to make things easier
normalizeChange :: Float -> Int
normalizeChange x = toInt $ x * fromIntegral 100

getMinCoins :: Float -> Int
getMinCoins change = calculateCoins (normalizeChange change) 0

calculateCoins :: Int -> Int -> Int
calculateCoins change coins
    | change >= quarter = calculateCoins (mod change quarter) (coins + div change quarter)
    | change >= dime = calculateCoins (mod change dime) (coins + div change dime)
    | change >= nickel  = calculateCoins (mod change nickel) (coins + div change nickel)
    | otherwise = change + coins
    where quarter = 25
          dime = 10
          nickel = 5

-- MAIN
main = do
    putStr "Change? "
    hFlush stdout
    answer <- getLine
    if null answer
        then main
    else do
        let result = getMinCoins $ toFloat answer
        printf "%d\n" result
