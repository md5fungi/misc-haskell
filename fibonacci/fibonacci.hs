-- ysrivastava

-- FUNCTION DECLARATIONS
 -- Math
gen_fibonacci :: Int -> Int
gen_fibonacci 0 = 0
gen_fibonacci 1 = 1
gen_fibonacci num = gen_fibonacci(num-1) + gen_fibonacci(num-2)

gen_fibonacci_lst num = map gen_fibonacci [0..num-1]

 -- Utils
list_to_str :: (Show a) => [a] -> String
list_to_str [x] = show x
list_to_str all@(_:xs) = list_to_str(init all) ++ " " ++ show(last all)

-- MAIN
main = do
    let list = gen_fibonacci_lst 5
    putStrLn(list_to_str list)
